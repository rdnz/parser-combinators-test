{- cabal:
default-language: GHC2021
default-extensions:
  NoFieldSelectors
  OverloadedStrings
ghc-options:
  -Weverything
  -Wno-unsafe
  -Wno-missing-safe-haskell-mode
  -Wno-missing-import-lists
  -Wno-missing-kind-signatures
  -Wno-implicit-prelude
build-depends:
  , base ==4.16.3.0
  , megaparsec ==9.3.0
  , text ==2.0.2
-}
{- project:
with-compiler: ghc-9.2.4
optimization: 0
-}

import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L
import Data.Void (Void)
import Data.Text (Text)

type Parser = Parsec Void Text

main :: IO ()
main = do
  -- backtracking
  parseTest
    ((pure ' ' <|> char 'a') *> char 'b' :: Parser Char)
    "ab"
  --   |
  -- 1 | ab
  --   | ^
  -- unexpected 'a'
  -- expecting 'b'
  parseTest
    (char 'a' *> char 'b' <|> pure ' ' :: Parser Char)
    "a"
  --   |
  -- 1 | a
  --   |  ^
  -- unexpected end of input
  -- expecting 'b'
  parseTest
    (try (char 'a' *> char 'b') <|> pure ' ' :: Parser Char)
    "a"
  -- ' '

  -- error information
  parseTest
    ((traverse char "abcde" <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting 'd'
  parseTest
    ((try (traverse char "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    (((try (traverse char "abcde") <?> "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'a' *> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'b' or 'c'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'b'
  parseTest
    ((try (char 'a' *> char 'b') <?> "ab") <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting ab

  parseTest ((many $ try $ L.decimal <* char '\n') <* eof :: Parser [Integer]) "98765\n4321O\n"
  --   |
  -- 2 | 4321O
  --   | ^
  -- unexpected '4'
  -- expecting end of input
  parseTest (L.decimal <* char '\n' <* eof :: Parser Integer) "4321O\n"
  --   |
  -- 1 | 4321O
  --   |     ^
  -- unexpected 'O'
  -- expecting digit or newline
  parseTest ((try (L.decimal <* char '\n') <|> pure 0) <* eof :: Parser Integer) "4321O\n"
  --   |
  -- 1 | 4321O
  --   | ^
  -- unexpected '4'
  -- expecting end of input
