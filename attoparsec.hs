{- cabal:
default-language: GHC2021
default-extensions:
  NoFieldSelectors
  OverloadedStrings
ghc-options:
  -Weverything
  -Wno-unsafe
  -Wno-missing-safe-haskell-mode
  -Wno-missing-import-lists
  -Wno-missing-kind-signatures
  -Wno-implicit-prelude
build-depends:
  , base ==4.16.3.0
  , attoparsec ==0.14.4
-}
{- project:
with-compiler: ghc-9.2.4
optimization: 0
-}

import Data.Attoparsec.Text
import Control.Applicative (Alternative ((<|>)), many)

main :: IO ()
main = do
  -- backtracking
  parseTest
    ((pure ' ' <|> char 'a') *> char 'b' :: Parser Char)
    "ab"
  --   |
  -- 1 | ab
  --   | ^
  -- unexpected 'a'
  -- expecting 'b'
  parseTest
    (char 'a' *> char 'b' <|> pure ' ' :: Parser Char)
    "a"
  -- ' '
  parseTest
    (try (char 'a' *> char 'b') <|> pure ' ' :: Parser Char)
    "a"
  -- ' '

  -- error information
  parseTest
    ((traverse char "abcde" <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    ((try (traverse char "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    (((try (traverse char "abcde") <?> "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   | ^
  -- unexpected 'a'
  -- expecting '.'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'a' *> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'c'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   | ^
  -- unexpected 'a'
  -- expecting 'c'
  parseTest
    ((try (char 'a' *> char 'b') <?> "ab") <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   | ^
  -- unexpected 'a'
  -- expecting 'c'
  parseTest (many (decimal <* char '\n') <* endOfInput) "98765\n4321O\n"
  --   |
  -- 2 | 4321O
  --   | ^
  -- unexpected '4'
  -- expecting end of input
  parseTest (decimal <* char '\n' <* endOfInput) "4321O\n"
  --   |
  -- 1 | 4321O
  --   |     ^
  -- unexpected 'O'
  -- expecting digit or newline
  parseTest ((decimal <* char '\n' <|> pure 0) <* endOfInput) "4321O\n"
  --   |
  -- 1 | 4321O
  --   | ^
  -- unexpected '4'
  -- expecting end of input
