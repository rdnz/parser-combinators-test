{- cabal:
default-language: GHC2021
default-extensions:
  NoFieldSelectors
  OverloadedStrings
ghc-options:
  -Weverything
  -Wno-unsafe
  -Wno-missing-safe-haskell-mode
  -Wno-missing-import-lists
  -Wno-missing-kind-signatures
  -Wno-implicit-prelude
build-depends:
  , base ==4.16.3.0
  , parsec ==3.1.16.1
-}
{- project:
with-compiler: ghc-9.2.4
optimization: 0
-}

import Text.Parsec
import Text.Parsec.Text

main :: IO ()
main = do
  -- backtracking
  parseTest
    ((pure ' ' <|> char 'a') *> char 'b' :: Parser Char)
    "ab"
  --   |
  -- 1 | ab
  --   | ^
  -- unexpected 'a'
  -- expecting 'b'
  parseTest
    (char 'a' *> char 'b' <|> pure ' ' :: Parser Char)
    "a"
  --   |
  -- 1 | a
  --   |  ^
  -- unexpected end of input
  -- expecting 'b'
  parseTest
    (try (char 'a' *> char 'b') <|> pure ' ' :: Parser Char)
    "a"
  -- ' '

  -- error information
  parseTest
    ((traverse char "abcde" <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting 'd'
  parseTest
    ((try (traverse char "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting 'd'
  parseTest
    (((try (traverse char "abcde") <?> "abcde") <|> pure "") <* char '.' :: Parser String)
    "abce."
  --   |
  -- 1 | abce.
  --   |    ^
  -- unexpected 'e'
  -- expecting abcde
  parseTest
    (try (char 'a' *> char 'b') <|> char 'a' *> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'c'
  parseTest
    (try (char 'a' *> char 'b') <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting 'b'
  parseTest
    ((try (char 'a' *> char 'b') <?> "ab") <|> char 'c' :: Parser Char)
    "ad"
  --   |
  -- 1 | ad
  --   |  ^
  -- unexpected 'd'
  -- expecting ab
